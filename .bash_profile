umask 077

[ ! -d "$HOME/.local/bin" ] && mkdir -p "$HOME/.local/bin"
export PATH="$PATH:$(du "$HOME/.local/bin" | cut -f2 | paste -sd ':')"

export XDG_CONFIG_HOME="${HOME}/.config"
export XDG_DATA_HOME="${HOME}/.local/share"
export XDG_CACHE_HOME="${HOME}/.cache"

[ ! -d "$XDG_CONFIG_HOME" ] && mkdir -p "$XDG_CONFIG_HOME"
[ ! -d "$XDG_DATA_HOME" ] && mkdir -p "$XDG_DATA_HOME"
[ ! -d "$XDG_CACHE_HOME" ] && mkdir -p "$XDG_CACHE_HOME"

export XDG_DESKTOP_DIR="$HOME/Files/Desktop"
export XDG_DOWNLOAD_DIR="$HOME/Files/Downloads"
export XDG_TEMPLATES_DIR="$HOME/Files/Templates"
export XDG_PUBLICSHARE_DIR="$HOME/Files/Public"
export XDG_DOCUMENTS_DIR="$HOME/Files/Documents"
export XDG_MUSIC_DIR="$HOME/Files/Music"
export XDG_PICTURES_DIR="$HOME/Files/Pictures"
export XDG_VIDEOS_DIR="$HOME/Files/Videos"
[ ! -d "$XDG_DESKTOP_DIR" ] && (mkdir -p "$XDG_DESKTOP_DIR")
[ ! -d "$XDG_DOWNLOAD_DIR" ] && (mkdir -p "$XDG_DOWNLOAD_DIR")
[ ! -d "$XDG_TEMPLATES_DIR" ] && (mkdir -p "$XDG_TEMPLATES_DIR")
[ ! -d "$XDG_TEMPLATES_DIR" ] && (mkdir -p "$XDG_TEMPLATES_DIR")
[ ! -d "$XDG_TEMPLATES_DIR" ] && (mkdir -p "$XDG_TEMPLATES_DIR")
[ ! -d "$XDG_MUSIC_DIR" ] && (mkdir -p "$XDG_MUSIC_DIR")
[ ! -d "$XDG_MUSIC_DIR" ] && (mkdir -p "$XDG_MUSIC_DIR")
[ ! -d "$XDG_MUSIC_DIR" ] && (mkdir -p "$XDG_MUSIC_DIR")


export XDG_MOUNT_DIR="$HOME/Files"

export EDITOR="nvim"
export VISUAL="nvim"
export TERMINAL="st"
export READER="zathura"
export BROWSER="brave"

export XAUTHORITY="${XDG_DATA_HOME}/Xauthority"
export WGETRC="${XDG_CONFIG_HOME}/wget/wgetrc"
echo hsts-file \= "$XDG_CACHE_HOME"/wget-hsts >> "$XDG_CONFIG_HOME/wget/wgetrc"
export ZDOTDIR="${XDG_CONFIG_HOME}/zsh/"
export HISTFILE="${XDG_DATA_HOME}/shell_history"
export HISTSIZE=
export SAVEHIST=
export LESSHISTFILE=-
export XMONAD_CONFIG_HOME="${XDG_CONFIG_HOME}/xmonad"
export XMONAD_CONFIG_DIR="${XDG_CONFIG_HOME}/xmonad"
export XMONAD_DATA_HOME="${XDG_DATA_HOME}/xmonad"
export XMONAD_CACHE_HOME="${XDG_CACHE_HOME}/xmonad"
export NPM_CONFIG_USERCONFIG="${XDG_CONFIG_HOME}/npm/npmrc"
export GTK2_RC_FILES="${XDG_CONFIG_HOME}/gtk-2.0/gtkrc-2.0"
export QT_QPA_PLATFORMTHEME="gtk2"
export MOZ_USE_XINPUT2="1"
export FZF_DEFAULT_COMMAND="find -L"

[ ! -d "${XDG_DATA_HOME}/vim" ] && mkdir -p "${XDG_DATA_HOME}"/vim/{undo,swap,backup}
[ ! -d "$XDG_CONFIG_HOME/wget" ] && (mkdir -p "$XDG_CONFIG_HOME/wget"; touch "$WGETRC")
[ ! -f "$XDG_CONFIG_HOME/git/config" ] && mkdir -p "$XDG_CONFIG_HOME/git" && touch "$XDG_CONFIG_HOME/git/config"

# Set vimrc's location and source it on vim startup
#export VIMINIT='let $MYVIMRC="${XDG_CONFIG_HOME}/vim/vimrc" | source $MYVIMRC'

[ ! -d "${XMONAD_CONFIG_HOME}" ] && mkdir "$XMONAD_CONFIG_HOME"
[ ! -d "${XMONAD_CONFIG_DIR}" ] && mkdir "$XMONAD_CONFIG_DIR"
[ ! -d "${XMONAD_DATA_HOME}" ] && mkdir "$XMONAD_DATA_HOME"
[ ! -d "${XMONAD_CACHE_HOME}" ] && mkdir "$XMONAD_CACHE_HOME"

[ ! -d "${ZDOTDIR}" ] && mkdir "$ZDOTDIR"

alias startx="startx ${XDG_CONFIG_HOME}/x11/xinitrc --:7: > /dev/null 2>&1 &"


# This is the list for lf icons:
export LF_ICONS="di=:\
fi=:\
tw=:\
ow=:\
ln=:\
or=:\
ex=:\
*.txt=:\
*.mom=:\
*.me=:\
*.ms=:\
*.png=:\
*.webp=:\
*.ico=:\
*.jpg=:\
*.jpe=:\
*.jpeg=:\
*.gif=:\
*.svg=:\
*.tif=:\
*.tiff=:\
*.xcf=:\
*.html=:\
*.xml=:\
*.gpg=:\
*.css=:\
*.pdf=:\
*.djvu=:\
*.epub=:\
*.csv=:\
*.xlsx=:\
*.tex=📜:\
*.md=:\
*.r=:\
*.R=:\
*.rmd=:\
*.Rmd=:\
*.m=:\
*.mp3=:\
*.opus=:\
*.ogg=:\
*.m4a=:\
*.flac=:\
*.mkv=:\
*.mp4=:\
*.webm=:\
*.mpeg=:\
*.avi=:\
*.zip=:\
*.rar=:\
*.7z=:\
*.tar.gz=:\
*.z64=🎮:\
*.v64=🎮:\
*.n64=🎮:\
*.gba=🎮:\
*.nes=🎮:\
*.gdi=🎮:\
*.1=:\
*.nfo=:\
*.info=:\
*.log=:\
*.iso=﫭:\
*.img=﫭:\
*.bib=🎓:\
*.ged=👪:\
*.part=💔:\
*.torrent=🔽:\
*.jar=:\
*.java=:\
"

#!/bin/sh

#input="$( echo "$@" | xargs -d'\n')"
#output=$(echo "$input" | awk -F'" "' '{print $NF}')
#output=$(echo "$output" | sed -e 's/^"//' -e 's/"$//')
#images=$(echo "$input" | awk -F'" "' '{$NF=""; print $0}')
#images=$(echo "$images" | sed -e 's/^"//' -e 's/"$//')
#
#notify-send "$images"
input=$(echo "$@" | xargs)
output="${1%.*}_compressed.pdf"

tppdf=$(mktemp /tmp/XXXX.pdf)

convert "$@" "$tppdf"

pdfSize="$(stat -c%s "$tppdf")"

tpps=$(mktemp /tmp/XXXX.ps)


while true; do
    orginSize="$(stat -c%s "$tppdf")"
    cp "$tppdf" "$tppdf.bak"
    pdf2ps "$tppdf" "$tpps"
    ps2pdf "$tpps" "$tppdf"
    outSize="$(stat -c%s "$tppdf")"
    if [ "$outSize" -ge "$orginSize" ]; then
        break
    fi
done
endSize="$orginSize"
percent="$(echo "$endSize" "$pdfSize" |awk '{printf "%.0f", $1 * 100 / $2}')"
minified="$((100-$percent))"
mv "$tppdf.bak" "$tppdf"

cp "$tppdf" "$output"
rm "$tppdf"
rm "$tpps"

#notify-send "File created named: $name.pdf. PDF reduced in size with $minified% to a file size of $(numfmt --to=si --suffix=B --round=nearest $endSize)"
exit 0

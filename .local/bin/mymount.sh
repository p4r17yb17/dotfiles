#!/bin/sh

# Edit the following file:
# /usr/share/polkit-1/actions/org.freedesktop.UDisks2.policy


if ! command -v udisksctl &> /dev/null
then
    echo "Install udisks2"
fi


unmount() {
    listed_devices="$(grep "/run/media" /proc/mounts  | awk '{print $2, "(" $1 ")" }' | sed "s/^\/run\/media\/$USER\///")"

    device="$(printf "$listed_devices\n" | dmenu -i -l 6  -p "Unmount:" )"

    if [ -n "$device" ];
    then
        device_name="$(echo $device | sed -e 's/\s(.*)$//g')"
        device_path="${device##* }"
        device_path=${device_path#"("}
        device_path=${device_path%")"}

        udisksctl unmount -b "$device_path"
        if [ "$?" = 0 ];
        then
            rm "${XDG_MOUNT_DIR:-/home/$USER}/$device_name"

            if command -v notify-send &> /dev/null
            then
                notify-send "mymount" "\"$device_path\" succesfull unmounted from \"${XDG_MOUNT_DIR:-/home/$USER}/$device_name\""
            fi
        fi

    fi
}


mount(){
    devices="$(lsblk -o path,label | tail -n +2)"
    listed_devices=""
    while IFS="" read -r p || [ -n "$p" ]
    do
        if ! grep -q "$(echo "$p"| awk '{print $1;}' )" /proc/mounts; then
            if [[ "$(echo "$p"| awk '{print $1;}' )" = *?[0-9] ]];
                then
                    listed_devices+="$(echo "$p"| awk '{first = $1; $1 = "";gsub(/^[ \t]+/,"",$0); if ($0) $0="("$0")"; print first, $0;}')\n"
            fi
        fi
    done <<< $devices
    device="$(printf "$listed_devices" | dmenu -i -l 6  -p "Mount:" )"

    if [ -n "$device" ];
    then

        device_path="$(echo $device | awk '{print $1 }')"
        message=$(udisksctl mount -b "$device_path")

        mount_path="$(echo "$message"| awk '{$1 = "";$2 = "";$3 = "";gsub(/^[ \t]+/,"",$0); print $0;}')"
        mount_name="$(echo "$mount_path" | sed "s/^\/run\/media\/$USER\///")"

        if command -v notify-send &> /dev/null
        then
            notify-send "mymount" "Mount \"$mount_path\" to \"${XDG_MOUNT_DIR:-/home/$USER}/$mount_name\""
        fi

        ln -s "$mount_path" "${XDG_MOUNT_DIR:-/home/$USER}/$mount_name"

    fi

}

usage(){
    echo "mymounter.sh [-m] [-u]"
}


while getopts "mu" o; do
    case "${o}" in
        u)
            unmount
            exit
            ;;
        m)
            mount
            exit
            ;;
        *)
            usage
            ;;
    esac
done

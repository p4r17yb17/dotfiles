# Dotfiles

![Desktop screenshot](https://gitlab.com/p4r17yb17/dotfiles/-/raw/master/.screenshots/tumb02.png)

# What Are Dotfiles?
Dotfiles are the customization files that are used to personalize your Linux or other Unix-based system.  You can tell that a file is a dotfile because the name of the file will begin with a period--a dot!  The period at the beginning of a filename or directory name indicates that it is a hidden file or directory.  This repository contains my personal dotfiles.  They are stored here for convenience so that I may quickly access them on new machines or new installs.  Also, others may find some of my configurations helpful in customizing their own dotfiles.

# My Window Manager Configs
- [XMonad](https://gitlab.com/p4r17yb17/dotfiles/-/tree/master/.config/xmonad)
- [Dwm](https://gitlab.com/p4r17yb17/dwm) (*)

(*) = *links to another repository of mine*

# Other Popular Configs Of Mine
- [St](https://gitlab.com/p4r17yb17/st) (*)
- [Dmenu](https://gitlab.com/p4r17yb17/dmenu) (*)
- [Neovim](https://gitlab.com/p4r17yb17/dotfiles/-/tree/master/.config/nvim)
- [Wallpapers](https://gitlab.com/p4r17yb17/wallpapers) (*)
- [Xmobar](https://gitlab.com/p4r17yb17/dotfiles/-/tree/master/.config/xmobar)

(*) = *links to another repository of mine*

# Themes I use

- [Arc-dark](https://chrome.google.com/webstore/detail/arc-dark/adicoenigffoolephelklheejpcpoolk) for Brave
- [Arc-Gruvbox](https://github.com/cyrinux/arc-gruvbox-theme) for GTK

# How To Manage Your Own Dotfiles
There are a hundred ways to manage your dotfiles. Personally, I use the *git bare repository method* for managing my dotfiles. Here is an article about this method of managing your dotfiles: [https://developer.atlassian.com/blog/2016/02/best-way-to-store-dotfiles-git-bare-repo/](https://developer.atlassian.com/blog/2016/02/best-way-to-store-dotfiles-git-bare-repo/)

# License
The files and scripts in this repository are licensed under the MIT License, which is a very permissive license allowing you to use, modify, copy, distribute, sell, give away, etc. the software. In other words, do what you want with it. The only requirement with the MIT License is that the license and copyright notice must be provided with the software.
